package actions

import java.time.LocalDateTime
import java.time.temporal.ChronoUnit

/**
 * RawToken, esta clase representa un token proporcionado por Gradebook
 * mantiene dos propiedades inmutables creadas con la instancia de la clase
 * tokenCreateTime , fecha de creacion del token
 * tokenExpireAt, fecha de explicacion q es la fecha actual + expireatseconds
 * @param username
 * @param accessToken
 * @param expireInSeconds
 */
@SerialVersionUID(123L)
class AuthenticationRawToken(
           val username:String,
           val accessToken:String,
           val expireInSeconds:Long
           ) extends Serializable{
  val tokenCreateTime = LocalDateTime.now()
  val tokenExpireAt = tokenCreateTime.plusSeconds(expireInSeconds)

  /**
   * isTokenExpired, determina si el token actual ha exipirado, a la fecha de expiración se le han restado
   * 5 minutos para evitar cualquier problema, por ejecutar una peticion con token expirado al server.
   * @return
   */
  def isTokenExpired(): Boolean ={
    val fixedExpireAt = tokenExpireAt.minusSeconds(Constantes.INVALIDATE_SUB_TIME)
    val currentDate = LocalDateTime.now()
    val delta = ChronoUnit.SECONDS.between(currentDate,fixedExpireAt)
    delta<=0L
  }

  /**
   * Constantes, companion object para almacenar constantes utiles para esta clase
   */
  @SerialVersionUID(127L)
  object Constantes extends Serializable{
    // invalidar los tokens, con 5 minutos de antelación
    val INVALIDATE_SUB_TIME:Long = 320
  }

  override def toString = accessToken
}
