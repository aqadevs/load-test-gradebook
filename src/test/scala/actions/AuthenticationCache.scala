package actions



import io.gatling.core.Predef._

object AuthenticationCache {

  def apply() = {

    val tokenStorage = AuthenticationTokenStorage.loadInstance()
    doIf(session => session("token").asOption[String].isEmpty) {
      exec(session => {
        var sessionMutable = session
        val currentUser = sessionMutable("username").as[String]
        val tokenSearchResult = tokenStorage.getTokenForUser(currentUser)

        if(tokenSearchResult.isDefined){
          val token = tokenSearchResult.get
          if(!token.isTokenExpired()){
            sessionMutable = sessionMutable.set("token", token)
              .set("expireInSeconds",token.expireInSeconds)
          }
        }
        sessionMutable
      })
    }
  }

}
