package actions




import io.gatling.core.Predef._
import org.slf4j.LoggerFactory


object DebugTokens {
  private val logger =  LoggerFactory.getLogger("aqas")
  def apply(session: Session):Session = {
    val currentUsername:String = session("username").as[String]
    val currentToken:String = session("token").as[String]
    logger.info("============================= SESSION ==============================")
    logger.info(s"Username [$currentUsername]")
    logger.info(s"Token [$currentToken]")
    logger.info("====================================================================")
    session
  }
}
