package actions



import actions.AuthenticationAction.random
import io.gatling.core.Predef._
import io.gatling.http.Predef._

import scala.util.Random

object UploadFileAction {
  val random = new Random()

  def apply() = {
    http("Cargar un archivo...")
      .post("/api/GradeRegistrationFromFile/UpdateFromExcel")
      .headers(Map("Authorization"->"Bearer ${token}",
                   "Content-Type"->"multipart/form-data; boundary=----WebKitFormBoundarynjzbUwThNuRBuitX",
                   "UserTest" -> "${username}"))
      .formParam("sectionCode", "${sectCodeInternet}")
      .formUpload("file","${file}")
      .check(bodyString.saveAs("BODY"))
      .check(status.is(s => 200))
  }
}
