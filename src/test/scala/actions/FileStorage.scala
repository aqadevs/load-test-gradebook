package actions

import java.io.{File, FileInputStream, FileOutputStream, ObjectInputStream, ObjectOutputStream}

import actions.AuthenticationTokenStorage.loadInstance
import io.gatling.core.Predef.Session

import scala.collection.mutable
import scala.collection.mutable.ArrayBuffer

/**
 * FileStorage, esta clase es serializable, se escribe y se actualiza en disco bajo el nombre
 * de FILE_NAME_STORAGE, esta clase mantiene un mapa con los rawFiles asociados a los usuarios bajo
 * test
 *
 *
 * Según los datos de prueba, un docente puede tener más asociado más de un curso, por esta razón cada descriptor
 * de archivo tiene asociado un contador de referencias, el cual se usuara para indicar cuando se ha tomado un archivo
 * más de una vez
 */
@SerialVersionUID(1234L)
class FileStorage extends Serializable {
  private val rawFileMaps: mutable.HashMap[Identifier, FileRawDescriptor] = new mutable.HashMap[Identifier, FileRawDescriptor]()
  val takedFiles:ArrayBuffer[FileRawDescriptor] = new ArrayBuffer[FileRawDescriptor]()

  /**
   * saverawFile, guarda el rawFile en cache
   *
   * @param rawFile
   */
  def saverawFile(rawFile: FileRawDescriptor): Unit = synchronized {
    val identifier = rawFile.id
    rawFileMaps.put(identifier, rawFile)
  }

  /**
   * getrawFileForUser,retorna el rawFile para un usuario
   * @return
   */
  def getFileAndMark(identifier: Identifier): Option[FileRawDescriptor] = {
    val file = rawFileMaps.get(identifier)
    if(file!=null){
      file.get.markAsSelected()
    }
    file
  }

  /**
   * getNextFile, retorna el siguien archivo para las pruebas tomando en cuenta
   * el numero de referencia de todos los archivos asociados a este docente.
   * @return
   */
  def getNextFileForUser(username: String): Option[FileRawDescriptor] = {
    val rusersf = rawFileMaps.values.filter((rawFile) => {
      val compare = rawFile.username == username
      compare
    })
    if(rusersf.isEmpty) return Option.empty
    Option(rusersf.reduceLeft(this.findMin))
  }

  def countDownloadFilesForUser(username:String):Int = {
    rawFileMaps.values.count((rawFile) => {
      val compare = rawFile.username == username
      compare
    })
  }


  /**
   * encuentra el minimo entre dos archivos usando el contador de referncias, esto es usado para
   * seleccionar un archivo por lo menos una
   * @param x
   * @param y
   * @return
   */
  def findMin(x: FileRawDescriptor, y: FileRawDescriptor): FileRawDescriptor = {
    val a = x.getNumReferences()
    val b = x.getNumReferences()

    val winner = a < b
    if(winner) return x else y
  }

  def hasrawFileByIden(identifier: Identifier): Boolean = {
    rawFileMaps.contains(identifier)
  }

  /**
   * indica si existe el archivo descargado para un usuario
   * @param username
   * @param sectCode
   * @return
   */
  def hasRawFile(username:String, sectCode:String):Boolean={
    val ident = new FileIdentifier(username,sectCode)
    return hasrawFileByIden(ident)
  }

  /**
   * cuenta el numero de archivos descargados para un usuario.
   * @param username
   * @return
   */
  def countDownloadedFilesForUser(username:String):Int={
    rawFileMaps.values.count((rawFile) => {
      val compare = rawFile.username == username
      compare
    })
  }

  /**
   * getAvaibleSectCodeFor, retorna los section code disponibles para la descarga, si un fichero ya fue descargado
   * se usara un sectcode que no forme parte de pruebas anteriores
   * @param username
   * @param allSectCodes
   * @return
   */
  def getAvaibleSectCodeFor(username:String, allSectCodes: Array[String]): Option[String] ={
      val selectedSectCodes = rawFileMaps.values.filter((rawFile) => {
        val compare = rawFile.username == username
        compare
      }).map(x=>x.sectCode).toArray

    val avaibleSectCodes = allSectCodes.diff(selectedSectCodes)
    if(!avaibleSectCodes.isEmpty){
      return Option(avaibleSectCodes(0))
    }else{
      return Option.empty
    }
  }

}

/**
 * Static methods, using singleton compatiopn object
 */
object FileStorage {
  private val FILE_NAME_STORAGE = "RAWFILES_SERIAL_CACHE";
  var srawFileStorage: FileStorage = null

  /**
   * Read all rawFiles from file store, if exists read into object serializable
   * @return
   */
  def loadInstance(): FileStorage = synchronized {
    if (srawFileStorage == null) {
      val file = new File(FILE_NAME_STORAGE)
      if (!file.exists()) {
        srawFileStorage = new FileStorage()
      } else {
        val ois = new ObjectInputStream(new FileInputStream(file))
        srawFileStorage = ois.readObject.asInstanceOf[FileStorage]
        ois.close()
      }
    }
    srawFileStorage
  }

  /**
   * serializeAndSave, write all rawFiles into serializabvle objectass
   * @param rawFileStorage
   */
  def serializeAndSave(rawFileStorage: FileStorage): Unit  = synchronized {
    val oos = new ObjectOutputStream(new FileOutputStream(FILE_NAME_STORAGE))
    oos.writeObject(rawFileStorage)
    oos.close()
  }


  def saveBinaryData(session: Session):Session={
    val instance = loadInstance()
    var sessionMutable = session
    val currentUsername:String = sessionMutable("username").as[String]
    val selectedSectCode =  sessionMutable("sectCodeInternet").as[String]
    val selectedPeriodo =  sessionMutable("selectedPeriodo").as[String]
    val fileContent = sessionMutable("filedownload").as[Array[Byte]]
    if(fileContent == null || fileContent.isEmpty){
      throw new IllegalStateException("File content cant be empty or null")
    }
    val testRawFile =  FileRawDescriptor.newInstance(
      userName = currentUsername,
      sectCode = selectedSectCode,
      periodo=selectedPeriodo
    );
    testRawFile.writeContent(fileContent)
    testRawFile.generateTestData()
    sessionMutable = sessionMutable.set("file",testRawFile.getAbsolutePath())
    instance.saverawFile(rawFile = testRawFile)
    serializeAndSave(instance)
    sessionMutable
  }



}
