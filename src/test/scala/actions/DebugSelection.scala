package actions



import io.gatling.core.Predef._

object DebugSelection {


  def apply(session: Session):Session = {
    val currentUsername:String = session("username").as[String]
    val currentFileLoad:String = session("file").as[String]
    val filePeriodo =  session("filePeriodo").as[String]
    val fileSectCode =  session("fileSectCode").as[String]
    val currentNumReferences:String = session("references").as[String]
    println("============================ CONTEXT RESULT USER SELECTION =========================")
    UserSimulation.setUsers.foreach((user)=>{
      println(s"USER [${user.username}] REFERENCES [${user.getNumReferences()}]")
    })
    println("========================================================================")
    return session
  }
}
