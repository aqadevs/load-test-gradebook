package actions


import java.util.concurrent.locks.{Lock, ReentrantLock}

import io.gatling.core.Predef._
import io.gatling.http.Predef._
import scala.util.control.Breaks._
import scala.collection.mutable

object GetUserSectionAction {
  private val selectedSections: mutable.HashMap[String, mutable.ArrayBuffer[String]] = new mutable.HashMap[String, mutable.ArrayBuffer[String]]()
  private[this] val lock :Lock = new ReentrantLock()

  /**+
   * Accion para la seleccion un periodo de forma aleatoría
   * @return
   */
  def apply() = {
    http("Obtener seleccion de usuario")
      //.get("/api/services/app/RegisterGrade/GetSectionUser?academicPeriodCode=${filePeriodo}")
      .get("/api/services/app/RegisterGradeTemplate/GetSectionUser?academicPeriodCode=${selectedPeriodo}")
      .headers(Map("Authorization" -> "Bearer ${token}",
        "UserTest" -> "${username}"))
      .check(status.is(200))
      .check(jsonPath("$.result").exists)
      .check(jsonPath("$.result.items").exists)
      .check(jsonPath("$.result.items[*].code").findAll.saveAs("allSectCodeInternet"))
      .check(jsonPath("$.result.items[?(@.campus=='PRESENCIAL')].code").findRandom.saveAs("sectCodeInternet"))



  }
  /**
   * putSelectedSection, Solo se permite la escritura de un hilo si más de un hilo intenta ingresar a este metodo
   * se pausara hasta haber registrado la seleccón del section code
   * @param username
   * @param section
   */
  def putSelectedSection(username:String, section:String): Unit ={
    lock.lock()
    try {
      if(!selectedSections.isDefinedAt(username)){
        var sections = mutable.ArrayBuffer(section)
        selectedSections.put(username,sections)
      }else{
        var sections = selectedSections(username)
        sections.append(section)
        selectedSections.put(username,sections)
      }
    }finally {
      lock.unlock()
    }

  }

  /**
   * isSectionAvaibleForPick, este metodo usa retrant Lock desde q las pruebas se ejecutan de forma concurrente
   * cuando existe más de un usuario con el mismo username en el test de pruebas; nigún otro hilo puede acceder a
   * este metodo si alguno esta escribiendo
   * @param username
   * @param section
   * @return
   */
  def isSectionAvaibleForPick(username:String, section:String): Boolean ={
    lock.lock()
    var avaibleForSelection = true
    try {
      var sections = selectedSections.get(username)
      if(sections.isDefined){
        var wasPicked = sections.get.contains(section)
        if(wasPicked){
          avaibleForSelection = false
        }
      }
    }finally {
      lock.unlock()
    }
    avaibleForSelection
  }


  /**
   * selectAvaibleSectCode, seleccionar un section Code disponible y que ademas no haya sido seleccionado
   * por nigún otro usuario con el mismo userna_name (registros reptidos en el test de carga)
   * @param session
   * @return
   */
  def selectAvaibleSectCode(session:Session): Session ={
    var mutableSession = session
    val sectCodeInternetResult = mutableSession("sectCodeInternet").asOption[String]
    val sectCodeInternetAllResult = mutableSession("allSectCodeInternet").asOption[Vector[String]]
    val currentUser = mutableSession("username").as[String]
    if(sectCodeInternetResult.isDefined){
      breakable{
        for(sectDowCode <- sectCodeInternetAllResult.get){
          val  avaibleSectCode = isSectionAvaibleForPick(currentUser,sectDowCode)
          if(avaibleSectCode){
            mutableSession = mutableSession.set("sectCodeInternet",sectDowCode)
            putSelectedSection(currentUser,sectDowCode)
            break()
          }
        }
      }
    }
    mutableSession
  }

  def shouldSearchNewSectCode(session:Session): Boolean = {
    var mutableSession = session
    val sectCodeInternetResult = mutableSession("sectCodeInternet").asOption[String]
    val sectCodeInternetAllResult = mutableSession("allSectCodeInternet").asOption[String]
    val currentUser = mutableSession("username").as[String]
    val fileStorage = FileStorage.loadInstance()

    if(sectCodeInternetAllResult.isEmpty || sectCodeInternetResult.isEmpty){
      return false
    }

    if(sectCodeInternetAllResult.get.isEmpty){
      return false
    }

    var isAvaible = GetUserSectionAction.isSectionAvaibleForPick(currentUser,sectCodeInternetResult.get)

    if(isAvaible) false else return true
  }


}
