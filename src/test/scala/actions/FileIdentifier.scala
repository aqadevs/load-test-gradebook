package actions

@SerialVersionUID(123L)
class FileIdentifier(
                    val username:String,
                    val sectCode:String
                    ) extends Identifier{
  override def get(): String = s"$username\\_$sectCode"
}
