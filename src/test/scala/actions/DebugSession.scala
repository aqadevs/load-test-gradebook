package actions



import java.io.FileWriter

import io.gatling.core.Predef._
import io.gatling.http.Predef._
import org.slf4j.LoggerFactory

import scala.util.Random

object DebugSession {
  private val logger = LoggerFactory.getLogger("aqas")
  def apply(session: Session):Session = {
    val currentUsername:String = session("username").as[String]
    val currentNumReferences:String = session("references").as[String]
    logger.info("============================ CONTEXT LOAD TEST =========================")
    logger.info(s"USERNAMNE [$currentUsername]")
    logger.info("references"+currentNumReferences)
    logger.info("========================================================================")
    session
  }
}
