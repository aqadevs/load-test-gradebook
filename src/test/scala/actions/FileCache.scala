package actions

import io.gatling.core.Predef._

/**
 * Al ejecutar el test, trata inciarlizar para la session actual de un usuario un archivo actualmente
 * descargado desde GRADE CORE
 * La Cache para un usuario sera invidalida cuando:
 * -  En la configuración exista mas usuarios con un mismo username que archivos descargadas
 */
object FileCache {

  def apply() = {

    val fileStorage = FileStorage.loadInstance()
    // Cuando la variable file en la session este vacia, tratar de cargar desde cache de archivos
    doIf(session => session("file").asOption[String].isEmpty) {
      exec(session => {
        var sessionMutable = session
        val currentUser = sessionMutable("username").as[String]
        val fileResult = fileStorage.getNextFileForUser(currentUser)


        if (fileResult.isDefined) {
          val file = fileResult.get
          if (file.exists()) {
            sessionMutable = sessionMutable.set("file", file.getAbsolutePath())
            sessionMutable = sessionMutable.set("sectCodeInternet", file.sectCode)
            sessionMutable = sessionMutable.set("selectedPeriodo", file.periodo)
          }
        }
        sessionMutable

      })
    }
  }

}
