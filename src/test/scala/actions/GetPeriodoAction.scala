package actions


import io.gatling.core.Predef._
import io.gatling.http.Predef._

object GetPeriodoAction {
  def apply() = {
    http("Obtener periodos para el usuario actualmente logeado")
      .get("/api/services/app/RegisterGrade/GetAcademicPeriod")
      .headers(Map("Authorization" -> "Bearer ${token}",
        "UserTest" -> "${username}"))
      .check(status.is(200))

      .check(jsonPath("$.result[0].id").transform((data:String)=>"202062").saveAs("selectedPeriodo"))
  }
}
