package actions



import io.gatling.core.Predef._
import io.gatling.core.feeder.FeederBuilder
import io.gatling.core.structure.ChainBuilder
import io.gatling.http.Predef._

import scala.util.Random

object AuthenticationAction {
  val random = new Random()

  def apply() = {
    http("Auth JWT Greenbook Test")
      .post("/api/TokenAuth/GradeRegisterAuthenticate")
      .header("Content-Type", "application/json")
      .body(StringBody("""{"userNameOrEmailAddress": "${username}" ,"password": "${password}" }"""))
      .check(jsonPath("$.result.accessToken").saveAs("token"))
      .check(jsonPath("$.result.expireInSeconds").saveAs("expireInSeconds"))
      .check(jsonPath("$.result.accessToken").transform((data)=>{
        println("Using user [$username]" + Thread.currentThread())
        "1"
      }).saveAs ("1"))
      .check(status.is(s => 200))
  }
}
