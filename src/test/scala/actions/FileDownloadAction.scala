package actions


import io.gatling.core.Predef._
import io.gatling.http.Predef._

object FileDownloadAction {


  def apply() = {
    http("Descargar un fichero")
      .get("/api/GradeRegistrationFromFile/DownloadTemplate?AcademicPeriodId=${selectedPeriodo}&SectionCode=${sectCodeInternet}&TemplateId=Calificaciones.v1.0.xlsm")
      //.header("Content-Type", "application/msexcel")
      .headers(Map("Authorization" -> "Bearer ${token}",
        "UserTest" -> "${username}"))
      .check(status.is(200))
      .check(bodyBytes.exists)
      .check(bodyBytes.saveAs("filedownload"))
      .check(status.is(200))

  }
}
