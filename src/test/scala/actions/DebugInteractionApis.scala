package actions



import io.gatling.core.Predef._
import io.gatling.http.Predef._
import scala.util.Random

object DebugInteractionApis {


  def apply(session: Session):Session = {
    val periodos:String = session("periodos").as[String]
    val currentToken = session("token").as[String]

    println("============================ CONTEXT INTERACTION =========================")
    println("Authentication " + currentToken)
    println("Periodos " + periodos)
    println("========================================================================")
    session
  }
}
