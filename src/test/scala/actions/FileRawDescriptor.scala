package actions

import java.io.{File, FileInputStream, FileOutputStream, InputStream, OutputStream}
import java.util.concurrent.atomic.AtomicInteger

import org.apache.poi.ss.usermodel._

@SerialVersionUID(223L)
class FileRawDescriptor(
               val username: String,
               val sectCode: String,
               val periodo: String,
               val fileName: String,
             ) extends Serializable {

  val id =  new FileIdentifier(username,sectCode)
  val file = new File(fileName)
  private val _random = new scala.util.Random
  private var atomicInteger: AtomicInteger = new AtomicInteger(0)
  private var selected: Boolean = false;

  /**
   * markAsSelected, marca a este usuario como seleccionado en la bateria de pruebas,
   * la restricción es no tomar un usuario repetido para poder hacer seguimiento en la
   * bateria de pruebas
   */
  def markAsSelected(): Unit = {
    atomicInteger.incrementAndGet()
    this.selected = true
  }

  def getNumReferences(): Int = {
    this.atomicInteger.get()
  }

  def exists(): Boolean ={
    return this.file.exists()
  }

  def getAbsolutePath():String = {
    file.getAbsolutePath
  }

  def writeContent(byteArray:Array[Byte]): Unit ={
    if(!file.exists()){
      file.getParentFile.mkdirs()
    }
    var f = new FileOutputStream(file)
    f.write(byteArray)
    f.close()
  }
  def generateTestData() = {
      val inp: InputStream = new FileInputStream(fileName)
      try { //InputStream inp = new FileInputStream("workbook.xlsx");
        val wb: Workbook = WorkbookFactory.create(inp)
        val sheet = wb.getSheetAt(0)
        var gradesRowStart = sheet.getRow(Constantes.START_GRADE_BLOCK)
        var gradesRowIndexStart = Constantes.START_GRADE_BLOCK;
        var gradesRowIndexEnd = getLasRowStudent(Constantes.START_GRADE_BLOCK, sheet);

        for (index <- gradesRowIndexStart to gradesRowIndexEnd) {
          val graderReg = sheet.getRow(index)
          for (grade <- Constantes.GRADES_ACTIVITIES) {
            val activityX = getOrCreateNoteCell(graderReg, grade)
            activityX.setCellValue(0 + _random.nextInt(10))
          }
        }
        try {
          val fileOut: OutputStream = new FileOutputStream(fileName)
          try wb.write(fileOut)
          finally {
            if (fileOut != null) fileOut.close()
          }
        } catch {
          case e: UnknownError => e.printStackTrace
        } finally {
          if (inp != null) inp.close()
        }
    }catch {
        case e: UnknownError => e.printStackTrace
    }
  }

  def getOrCreateNoteCell(row: Row, cellIndex: Int): Cell = {
    var cellElement = row.getCell(cellIndex)
    if (cellElement == null) {
      cellElement = row.createCell(Constantes.COLUMN_1_BIM_ACTIVITU_1)
    }
    cellElement
  }

  def getLasRowStudent(starRow: Int, sheet: Sheet): Int = {
    var avavibleIndex = starRow;
    var gradesRowStart = sheet.getRow(avavibleIndex)
    var studentCell = gradesRowStart.getCell(Constantes.COLUMN_STUDENTS_DNI)
    while (!isCellEmpty(studentCell)) {
      avavibleIndex = avavibleIndex + 1;
      gradesRowStart = sheet.getRow(avavibleIndex)
      studentCell = gradesRowStart.getCell(Constantes.COLUMN_STUDENTS_DNI)
    }
    avavibleIndex - 1
  }

  def isCellEmpty(cell: Cell): Boolean = {
    if (cell == null) { // use row.getCell(x, Row.CREATE_NULL_AS_BLANK) to avoid null cells
      return true
    }
    if (cell.getCellType eq CellType.BLANK) return true
    if ((cell.getCellType eq CellType.STRING) && cell.getStringCellValue.trim.isEmpty) return true
    false
  }

  @SerialVersionUID(2251L)
  object Constantes extends Serializable{
    val START_GRADE_BLOCK = 15;
    val COLUMN_STUDENTS_DNI = 1;
    val COLUMN_1_BIM_ACTIVITU_1 = 5;
    val COLUMN_1_BIM_ACTIVITU_2 = 6;
    val COLUMN_1_BIM_ACTIVITU_3 = 8;
    val COLUMN_1_BIM_ACTIVITU_4 = 9;
    val COLUMN_1_BIM_ACTIVITU_5 = 11;
    val COLUMN_1_BIM_ACTIVITU_6 = 12;
    val COLUMN_1_BIM_ACTIVITU_7 = 15;
    val COLUMN_1_BIM_ACTIVITU_8 = 16
    val COLUMN_1_BIM_ACTIVITU_9 = 17;
    val COLUMN_1_BIM_ACTIVITU_10 = 18
    val COLUMN_1_BIM_ACTIVITU_11 = 20;
    val COLUMN_1_BIM_ACTIVITU_12 = 21

    val GRADES_ACTIVITIES = Array(
      COLUMN_1_BIM_ACTIVITU_1,
      COLUMN_1_BIM_ACTIVITU_2,
      COLUMN_1_BIM_ACTIVITU_3,
      COLUMN_1_BIM_ACTIVITU_4,
      COLUMN_1_BIM_ACTIVITU_5,
      COLUMN_1_BIM_ACTIVITU_6,
      //COLUMN_1_BIM_ACTIVITU_7,
      //COLUMN_1_BIM_ACTIVITU_8,
      //COLUMN_1_BIM_ACTIVITU_9,
      //COLUMN_1_BIM_ACTIVITU_10,
      //COLUMN_1_BIM_ACTIVITU_11,
      //COLUMN_1_BIM_ACTIVITU_12
    )
  }

}
object FileRawDescriptor{
  def newInstance(userName:String,sectCode:String, periodo:String): FileRawDescriptor ={
    val fileName = s"files/Calificaciones-${sectCode}-${periodo}.xlsm"
    val rawFile = new FileRawDescriptor(
      username=userName,
      sectCode=sectCode,
      periodo=periodo,
      fileName= fileName,
    )
    return rawFile
  }
}
