import actions._
import io.gatling.core.Predef._
import io.gatling.http.Predef._

/**
 * Auth, esta simulacíón es usada para incializar todos los tokens de un usuario antes de ejecutar las pruebas de carga,
 * una vez ejecutada esta simulacíón todos los tokens seran almacenados e indexados para cada usuario, para que este
 * proceso no afecte el test de carga de archivos.
 *
 * Se intenta Obtener un token con 3 reintentos maximos por docente.
 */
class Auth extends Simulation {
  /**
   * ======================= HTTP PROTOCOL GLOBAL CONFIG=====================
   */
  val httpProtocol = http
    .baseUrl(ConfigTest.baseUrl)
    .inferHtmlResources(black = BlackList(""".*\.js""", """.*\.css"""))
    .acceptHeader("text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8")

  val selectUser = Iterator.continually(UserSimulation.getNextUser().getAsMap())
  val authTokenStorage = AuthenticationTokenStorage.loadInstance()
  val downloadFileFlow = scenario("Gradebook downloadFlow ")
    .exec(feed(selectUser))
    .exec(sessionFunction = (session) => DebugSession(session))
    // trata de recuperar eltoken  desde cache
    .exec(AuthenticationCache())
    // Si no se pudo incializar el token se procede a logearse
    .doIf(session => session("token").asOption[String].isEmpty) {
      tryMax(3) {
        exec(AuthenticationAction())
          .exec((session) => AuthenticationTokenStorage.saveInCache(session))
          .pause(10,15)
      }
    }

  // Ejecución de la descarga de archivos, tomando como configuración lo definido en .conf
  setUp(
    downloadFileFlow.inject(atOnceUsers(ConfigTest.peekUsers)),
  ).protocols(httpProtocol)
}

