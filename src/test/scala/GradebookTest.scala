import actions._
import io.gatling.core.Predef._
import io.gatling.core.session.Session
import io.gatling.http.Predef._
import org.slf4j.LoggerFactory

/**
 * GradebookTest, esta simulación se enfoca integra dos simulaciones anterios
 * : Authenticacion
 * : Descarga de archivos
 * : Carga de archivos
 */
class GradebookTest extends Simulation {

  val httpProtocol = http
    .baseUrl(ConfigTest.baseUrl)
    .inferHtmlResources(black = BlackList(""".*\.js""", """.*\.css"""))
    .acceptHeader("text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8")

  val selectUser = Iterator.continually(UserSimulation.getNextUser().getAsMap())

  val lc = AuthenticationTokenStorage.loadInstance()

  val testGeneral = scenario("Gradebook AQAS TEST")
    .exec(feed(selectUser))
    .exec(sessionFunction = (session) => DebugSession(session))
    .exec(AuthenticationCache())
    .doIf(session => session("token").asOption[String].isEmpty) {
      tryMax(2) {
        exec(AuthenticationAction())
          .exec((session) => AuthenticationTokenStorage.saveInCache(session))
      }
    }
    .exec(FileCache())
    .exec(UploadFileAction())

  setUp(
    //    testGeneral.inject(
    ////      incrementUsersPerSec(1) // Double
    ////        .times(2)
    ////        .eachLevelLasting(FiniteDuration.apply(5,TimeUnit.SECONDS))
    //        //.separatedByRampsLasting(FiniteDuration.apply(10,TimeUnit.SECONDS))
    //        //.startingFrom(1) // Double
    //    ) ,

    testGeneral.inject(
      atOnceUsers(5),
    )
  ).protocols(httpProtocol)
}

