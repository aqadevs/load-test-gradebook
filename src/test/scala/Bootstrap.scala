import actions._
import io.gatling.core.Predef._
import io.gatling.http.Predef._

/**
 * Bootstrap, esta simulacíón es usada para simular el comportamiento de descarga de archivos
 * por parte de los docentes,puesto que es un escenario del cual dependen otros casos de prueba, los archivos se descargaran
 * y se indexaran en una cache local para el posterior uso en el escenarios de carga.
 * de archivos.
 */
class Bootstrap extends Simulation {
  /**
   * ======================= HTTP PROTOCOL GLOBAL CONFIG=====================
   */
  val httpProtocol = http
    .baseUrl(ConfigTest.baseUrl)
    .inferHtmlResources(black = BlackList(""".*\.js""", """.*\.css"""))
    .acceptHeader("text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8")

  val selectUser = Iterator.continually(UserSimulation.getNextUser().getAsMap())
  val authTokenStorage = AuthenticationTokenStorage.loadInstance()
  val downloadFileFlow = scenario("Gradebook downloadFlow ")
    .exec(feed(selectUser))
    .exec(sessionFunction = (session) => DebugSession(session))
    // trata de recuperar eltoken  desde cache
    .exec(AuthenticationCache())
    // Si no se pudo incializar el token se procede a logearse
    .doIf(session => session("token").asOption[String].isEmpty) {
      tryMax(3) {
        exec(AuthenticationAction())
          .exec((session) => AuthenticationTokenStorage.saveInCache(session))
      }
    }
    // Se trata de cargar el archivo desde cache
    .exec(FileDownloadCache())
    // Si se determina q la cache no es apta o valida se procede a descargar
    .doIf(session => session("file").asOption[String].isEmpty) {
      exec(GetPeriodoAction())
        .exec(GetUserSectionAction())
        .exec(session => GetUserSectionAction.selectAvaibleSectCode(session))
        .exec(exec(FileDownloadAction()))
        .exec((session) => FileStorage.saveBinaryData(session))
    }

  // Ejecución de la descarga de archivos, tomando como configuración lo definido en .conf
  setUp(
    downloadFileFlow.inject(atOnceUsers(ConfigTest.peekUsers)),
  ).protocols(httpProtocol)
}

