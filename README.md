### Solo ejecución

#### Cambios
La simulación fue dividida en 3 partes
- Auth
- Descargar y generación de contenido para los archivos
- Carga de calificaciones


No necesitarais de ninguna configuración, solo ejecutar usando el wrapper de windows
para maven
```bash
     // Configurar
     mvnw gatling:test -Dgatling.simulationClass=Bootstrap   
     // iniciar test        
     mvnw gatling:test
```

## Opcionales full soporte 
- JVM 1.8 Minino
- Maven
- Scala
- intelljidea IDE

## Configuraciones

Las configuraciones como HOST, usuarios archivos se encuentran en el archivo simulation.conf 

####
Modificar GreenBookTest.scala 
```$xslt
    testGeneral.inject(
      atOnceUsers(100)
      //constantUsersPerSec(5) during (FiniteD
```

#### Ejecutar

```bash
     mvn gatling:test
```

### Estructura de directorios

```tree

load-test.iml
├── mvnw
├── mvnw.cmd
├── pom.xml
├── README.md
├── src
│   └── test
│       ├── resources  #Ficheros de prueba y ajustes
│       │   ├── bodies
│       │   ├── Calificaciones-20001-202062.xlsm
│       │   ├── Calificaciones-20003-202062.xlsm
│       │   ├── Calificaciones-20004-202062.xlsm
│       │   ├── Calificaciones-20005-202062.xlsm
│       │   ├── data
│       │   ├── gatling.conf   # configuraciones framework
│       │   ├── logback.xml
│       │   ├── RCalif.xlsm   
│       │   ├── recorder.conf   
│       │   └── simulation.conf    # Configuraciones de la simulacion
│       └── scala
│           ├── actions  # acciones
│           ├── Engine.scala
│           ├── GreenBookTest.scala  ## MAIN 
│           ├── IDEPathHelper.scala
│           ├── Recorder.scala
└── TOKENS_SERIAL_CACHE
```

